defmodule HelloNerves.Http do
  use Plug.Router

  plug(:match)
  plug(:dispatch)

  get "/toggle" do
    HelloNerves.LedState.toggle()
    send_resp(conn, 200, "LED toggled")
  end

  match(_, do: send_resp(conn, 404, "Oops!"))
end
