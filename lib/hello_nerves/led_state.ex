defmodule HelloNerves.LedState do
  use GenServer

  @moduledoc """
    Simple GenServer to control GPIO #18.
  """

  require Logger
  alias Circuits.GPIO

  def start_link(state \\ []) do
    GenServer.start_link(__MODULE__, state, name: __MODULE__)
  end

  def init(_state) do
    {:ok, output_gpio} = GPIO.open(4, :output)

    {:ok, input_gpio} = GPIO.open(14, :input)
    :ok = Circuits.GPIO.set_interrupts(input_gpio, :both)

    {:ok, {output_gpio, 0}}
  end

  def handle_cast(:toggle, {output_gpio, old_state}) do
    new_state = rem(old_state + 1, 2)
    GPIO.write(output_gpio, new_state)
    {:noreply, {output_gpio, new_state}}
  end

  def toggle() do
    GenServer.cast(__MODULE__, :toggle)
  end

  def handle_info({:circuits_gpio, 14, time, _}, {output_gpio, old_state}) do
    GPIO.write(output_gpio, 0)

    {:noreply, {output_gpio, 0, time}}
  end

end
